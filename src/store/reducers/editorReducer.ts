import { IStandaloneCodeEditor } from "swat/editor/MonacoEditor";
import { Action, GenericAction } from "swat/store";

export enum EditorActionsEnum {
    LOAD = "LOAD_EDITOR",
    UNLOAD = "UNLOAD_EDITOR",
}

interface LoadEditorAction {
    type: EditorActionsEnum.LOAD;
    editor: IStandaloneCodeEditor;
}

export type EditorAction =
    | LoadEditorAction
    | GenericAction<EditorActionsEnum.UNLOAD>;

export type EditorState = IStandaloneCodeEditor | null;

export default function editorReducer(
    state: EditorState = null,
    action: Action
): EditorState {
    switch (action.type) {
        case EditorActionsEnum.LOAD:
            return action.editor;
        case EditorActionsEnum.UNLOAD:
            return null;
        default:
            return state;
    }
}
