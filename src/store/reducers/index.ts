import { combineReducers } from "redux";
import tabsReducer from "swat/store/reducers/tabsReducer";
import editorReducer from "./editorReducer";

export default combineReducers({
    tabState: tabsReducer,
    editor: editorReducer,
});
