import { monaco, Tab } from "swat/editor";
import Revision from "swat/mediawiki/Revision";
import store, { Action, GenericAction } from "swat/store";
import { isUndefined, propIsUndefined } from "swat/util";

export interface TabState {
    activeTab: string | null;
    tabs: Tab[];
}
export const EmptyTabState: TabState = { activeTab: null, tabs: [] };

export default function tabsReducer(
    state = EmptyTabState,
    action: Action
): TabState {
    switch (action.type) {
        case TabActionsEnum.NEW:
            saveActiveTab(state);
            if (verifyRevision(action.revision)) {
                const newTab = new Tab(action.revision);
                const activeTab = newTab.getId();
            }
            return state;
        case TabActionsEnum.CLOSE:
            return {
                activeTab: null,
                tabs: state.tabs.filter(
                    (tab) => !action.ids.includes(tab.getId())
                ),
            };
        case TabActionsEnum.OPEN:
            saveActiveTab(state);
            const activeTab = action.id;
            store
                .getState()
                .editor.restoreViewState(
                    getTab(
                        state,
                        activeTab
                    ).getViewState() as monaco.editor.ICodeEditorViewState
                );
            return { activeTab, tabs: state.tabs };
        case TabActionsEnum.SAVE:
            saveActiveTab(state);
            return state;
        case TabActionsEnum.CLOSEALL:
            return { activeTab: null, tabs: [] };
        default:
            return state;
    }
}

function verifyRevision(revision?: Revision): boolean {
    if (isUndefined(revision)) {
        return false;
    }
    const hasPage =
        !propIsUndefined(revision, "pageid") ||
        !propIsUndefined(revision, "page");
    const hasId =
        !propIsUndefined(revision, "revid") ||
        !propIsUndefined(revision, "parentid");
    const hasContent = !propIsUndefined(revision, "content");
    return hasContent || (hasPage && hasId);
}

function saveActiveTab(state: TabState): void {
    getActiveTab(state).saveState();
}

export function getActiveTab(state: TabState): Tab {
    return state.tabs[getActiveTabIndex(state)];
}

export function getActiveTabIndex(state: TabState): number {
    return getTabIndex(state, state.activeTab);
}

export function getTab(state: TabState, id: string): Tab {
    return state.tabs[getTabIndex(state, id)];
}

export function getTabIndex(state: TabState, id: string): number {
    return state.tabs.findIndex((tab) => tab.getId() === id);
}

export enum TabActionsEnum {
    NEW = "NEW_TAB",
    CLOSE = "CLOSE_TABS",
    CLOSEALL = "CLOSE_ALL_TABS",
    OPEN = "OPEN_TAB",
    SAVE = "SAVE_TAB",
}

interface NewTabAction {
    type: TabActionsEnum.NEW;
    revision?: Revision;
}

interface CloseTabAction {
    type: TabActionsEnum.CLOSE;
    ids: string[];
}

/** Not to be confused with NewTabAction, this opens an existing tab instead of making a new one. */
interface OpenTabAction {
    type: TabActionsEnum.OPEN;
    id: string;
}

export type TabAction =
    | NewTabAction
    | CloseTabAction
    | GenericAction<TabActionsEnum.CLOSEALL>
    | OpenTabAction
    | GenericAction<TabActionsEnum.SAVE>;
