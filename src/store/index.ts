//import { getDefaultMiddleware } from "@reduxjs/toolkit";
import { createStore, applyMiddleware } from "redux";
import { composeWithDevTools } from "redux-devtools-extension/developmentOnly";
import reducer from "./reducers";
import {
    EmptyTabState,
    TabAction,
    TabActionsEnum,
    TabState,
} from "./reducers/tabsReducer";
import {
    EditorAction,
    EditorActionsEnum,
    EditorState,
} from "./reducers/editorReducer";

export interface IState {
    tabState: TabState;
    editor: EditorState;
}

export const EmptyStore: IState = { tabState: EmptyTabState, editor: null };

export type Action = TabAction | EditorAction;

// https://stackoverflow.com/a/55827534/12573645
export const Actions = { ...TabActionsEnum, ...EditorActionsEnum };
export type Actions = typeof Actions;

const store = createStore<
    IState,
    Action,
    Record<string, never>,
    Record<string, never>
>(
    reducer,
    EmptyStore,
    composeWithDevTools(applyMiddleware(/* ...getDefaultMiddleware() */))
);
export default store;

export interface GenericAction<T extends string> {
    type: T;
}
