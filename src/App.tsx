import React from "react";
import MonacoEditor from "swat/editor";
import TabList from "swat/ui";

export default class App extends React.Component {
    render(): JSX.Element {
        return (
            <>
                <TabList id="tabBar" />
                <MonacoEditor
                    options={{
                        language: "markdown",
                        theme: "vs-dark",
                        automaticLayout: true,
                    }}
                    id="container"
                >{`=Hello World!=
This is an example wikitext file. As you can see, it is very complex and wasn't made in 5 minutes.
<!-- This is a comment! We use __TOC__ so that the language server can detect keywords. We also put __TOC__ in this comment to test that keywords are ignored in comments. -->
__TOC__
==Heading Detection (should be in TOC)==
Hello! This has complex styling like ''italics'', '''bold''', ''''both'''', and <code>code!</code>

===Another Heading===
{{POV section}}
This section is very cool and has cool templates! {{POV statement}}
{| class="wikitable"
|+This table can be edited with a GUI!
|-
! Header 1 !! Header 2
|-
| Some text || Hello World!!
|-
| Tables are cool! || 1+1=2!
|}

==Wikilinks==
[[H:WIKILINK|Wikilinks]] are cool! So are [[WP:PIPETRICK|]]s!

==Talk behavior==
I can sign my name like this: ~~~~

{{stub}}
`}</MonacoEditor>
            </>
        );
    }
}
