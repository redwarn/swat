import { Environment } from "monaco-editor";
declare global {
    interface Window {
        MonacoEnvironment: Environment | undefined;
    }
}
declare module "*.scss";
declare module "*.module.scss" {
    const classes: { [key: string]: string };
    export default classes;
}
