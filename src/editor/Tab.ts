import Revision from "swat/mediawiki/Revision";
import { generateId } from "swat/util";
import { IEditorViewState } from "./MonacoEditor";
import store from "swat/store";

export class Tab {
    private viewState: IEditorViewState;
    public active = false;
    constructor(public revision: Revision) {
        this.id = generateId(15);
    }

    private id: string;
    getId(): string {
        return this.id;
    }

    saveState(): void {
        if (this.active) {
            this.viewState = store.getState().editor.saveViewState();
        }
    }

    getViewState(): IEditorViewState {
        return this.viewState;
    }
}
