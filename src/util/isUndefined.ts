export default function isUndefined(obj: unknown): boolean {
    return obj === null || obj === void 0;
}

export function propIsUndefined<T>(obj: T, prop: keyof T): boolean {
    return isUndefined(obj[prop]);
}
