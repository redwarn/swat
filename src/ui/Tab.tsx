import * as React from "react";
import { Tab as MonacoTab } from "swat/editor";
import styles from "./assets/Tab.module.scss";
import { connect, ConnectedProps } from "react-redux";

import { IState } from "swat/store";

const mapState = (state: IState) => ({ tabState: state.tabState });

const connector = connect(mapState);

type ReduxProps = ConnectedProps<typeof connector>;

export interface TabProps extends ReduxProps {
    tab: MonacoTab;
}

class Tab extends React.Component<TabProps> {
    render(): JSX.Element {
        return (
            <div
                className={
                    this.props.tab.active ? styles.activeTab : styles.tab
                }
            ></div>
        );
    }
}

export default connector(Tab);
