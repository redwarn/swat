import * as React from "react";
import { connect, ConnectedProps } from "react-redux";

import { IState } from "swat/store";
import Tab from "./Tab";

const mapState = (state: IState) => ({ tabState: state.tabState });

const connector = connect(mapState);

type ReduxProps = ConnectedProps<typeof connector>;

export interface TabListProps extends ReduxProps {
    id?: string;
    className?: string;
}

class TabList extends React.Component<TabListProps> {
    render(): JSX.Element {
        return (
            <div
                {...(this.props.id ? { id: this.props.id } : {})}
                {...(this.props.className
                    ? { className: this.props.className }
                    : {})}
            >
                {this.props.tabState.tabs.map((monacoTab) => (
                    <Tab tab={monacoTab} />
                ))}
            </div>
        );
    }
}
export default connector(TabList);
