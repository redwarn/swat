const path = require("path");
const webpack = require("webpack");

const TerserPlugin = require("terser-webpack-plugin");
const CopyPlugin = require("copy-webpack-plugin");

module.exports = {
    mode: "development",
    entry: {
        "main": "./src/index.tsx",
        "editor.worker": "monaco-editor/esm/vs/editor/editor.worker.js",
    },
    plugins: [
        new webpack.ProgressPlugin(),
        new CopyPlugin({
            patterns: ["src/assets/index.html", "src/assets/favicon.svg"],
        }),
    ],

    module: {
        rules: [
            {
                test: /\.(ts|tsx)$/,
                loader: "ts-loader",
                include: [path.resolve(__dirname, "src")],
                exclude: [/node_modules/],
            },
            {
                test: /.(scss|css)$/,

                use: [
                    {
                        loader: "style-loader",
                    },
                    {
                        loader: "css-loader",

                        options: {
                            sourceMap: true,
                            modules: "global",
                        },
                    },
                    {
                        loader: "sass-loader",

                        options: {
                            sourceMap: true,
                        },
                    },
                ],
            },
            {
                test: /\.ttf$/,
                use: ["file-loader"],
            },
        ],
    },

    resolve: {
        extensions: [".tsx", ".ts", ".js"],
        alias: {
            swat: path.resolve(__dirname, "src/"),
        },
    },

    optimization: {
        minimizer: [new TerserPlugin()],

        splitChunks: {
            cacheGroups: {
                vendors: {
                    priority: -10,
                    test: /[\\/]node_modules[\\/]/,
                },
            },

            chunks: "async",
            minChunks: 1,
            minSize: 30000,
            name: false,
        },
    },

    output: {
        globalObject: "self",
        filename: "[name].bundle.js",
        path: path.resolve(__dirname, "dist"),
    },
};
